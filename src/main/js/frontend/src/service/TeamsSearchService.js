import axios from 'axios'

const TEAMS_API_URL = 'http://localhost:8080'
const COUNTRIES_API_URL = `${TEAMS_API_URL}/countries`

export class TeamsSearchService {

    retrieveAllTeams(name) {
        return axios.get(`${COUNTRIES_API_URL}/${name}/teams`);
    }
}