import React, { Component } from 'react';
import './App.css';
import TeamsApp from './components/TeamsApp';

class App extends Component {
    render() {
        return (
            <div className="container">
                <TeamsApp />
            </div>
    );
    }
}

export default App;