import React, {Component} from 'react';
import ListTeamsComponent from "./ListTeamsComponent";

class TeamsApp extends Component {
    render() {
        return (<>
                <h1 className="AppLabel" >Football teams application</h1>
                <ListTeamsComponent/>
            </>
        )
    }
}

export default TeamsApp;