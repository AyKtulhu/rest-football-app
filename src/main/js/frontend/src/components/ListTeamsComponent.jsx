import 'primeicons/primeicons.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';


import React, {Component} from 'react';
import {TeamsSearchService} from "../service/TeamsSearchService";
import {DataTable} from 'primereact/datatable';
import {Column} from "primereact/column";
import {AutoComplete} from "primereact/autocomplete";


class ListTeamsComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            country: "Brazil",
            filteredCountries: null,
            selectedCustomers: null,
            teams: [],
            message: null
        }
        this.filteredCountries = this.filteredCountries.bind(this);
        this.service = new TeamsSearchService();
    }

    componentDidMount() {
        this.refreshTeams("Brazil");
        this.countries = ["Brazil", "England", "Netherlands"];
    }

    refreshTeams(country) {
        this.service.retrieveAllTeams(country)
            .then(
                response => {
                    this.setState({teams: response.data})
                }
            )
    }

    filteredCountries(event) {
        setTimeout(() => {
            let results;

            if (event.query.length === 0) {
                results = [...this.countries];
            }
            else {
                results = this.countries.filter((c) => {
                    return c.toLowerCase().startsWith(event.query.toLowerCase());
                });
            }

            this.setState({ filteredCountries: results });
        }, 250);
    }

    render() {

        return (
            <div className="datatable-doc-demo">
                <AutoComplete value={this.state.country}
                              suggestions={this.state.filteredCountries}
                              completeMethod={this.filteredCountries}
                              size={4} minLength={1}
                              dropdown={true}
                              onChange={(e) => {
                                  this.setState({country: e.value});
                                  this.refreshTeams(e.value)
                              } }
                />
                <DataTable value={this.state.teams}
                           dataKey="id"
                           className="p-datatable-customers"
                           paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                           selection={this.state.selectedCustomers}
                           onSelectionChange={e => this.setState({selectedCustomers: e.value})}
                           paginator rows={10}
                           emptyMessage="No teams found"

                >
                    <Column field="team_id"
                            header="Id"
                            style={{ width: "5%" }}/>
                    <Column sortField="name"
                            filterField="name"
                            sortable filter filterMatchMode="contains"
                            field="name"
                            header="name"/>
                    <Column sortField="founded"
                            filterField="founded"
                            style={{width: "10%"}}
                            sortable filter filterMatchMode="contains"
                            field="founded"
                            header="Founded"/>
                    <Column sortField="venue_name"
                            filterField="venue_name"
                            sortable filter filterMatchMode="contains"
                            field="venue_name"
                            header="Venue name"/>
                    <Column sortField="venue_surface"
                            filterField="venue_surface"
                            sortable filter filterMatchMode="contains"
                            field="venue_surface"
                            header="Venue surface"/>
                    <Column sortField="venue_address"
                            filterField="venue_address"
                            sortable filter filterMatchMode="contains"
                            field="venue_address"
                            header="Venue address"/>
                    <Column sortField="venue_capacity"
                            sortable
                            field="venue_capacity"
                            header="Venue capacity"/>
                </DataTable>
            </div>
        )
    }
}

export default ListTeamsComponent;