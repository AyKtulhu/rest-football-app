package ru.trueim.restfootballapp.services;

import ru.trueim.restfootballapp.dto.Team;

import java.util.List;

public interface SearchTeamService {

    List<Team> getTeamsByCountry(String countryName);

}
