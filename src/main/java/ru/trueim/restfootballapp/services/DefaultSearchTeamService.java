package ru.trueim.restfootballapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.trueim.restfootballapp.dto.Api;
import ru.trueim.restfootballapp.dto.Team;

import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultSearchTeamService implements SearchTeamService{

    @Value("${resource.tasks}/{country}")
    private String nameResource;
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Team> getTeamsByCountry(String countryName) {
        Api api = restTemplate.getForObject(nameResource, Api.class, countryName);
        if(api == null){
            return new ArrayList<>();
        }
        return (api.getApi() == null) ? new ArrayList<>() : api.getApi().getTeams();
    }
}
