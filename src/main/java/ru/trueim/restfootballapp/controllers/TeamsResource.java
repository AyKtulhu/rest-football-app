package ru.trueim.restfootballapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.trueim.restfootballapp.dto.Team;
import ru.trueim.restfootballapp.services.SearchTeamService;

import java.util.List;

@CrossOrigin(origins = { "http://localhost:3000"})
@RestController
public class TeamsResource {

    @Autowired
    private SearchTeamService teamService;

    @GetMapping("/countries/{countryName}/teams")
    public List<Team> getCountryTeams(@PathVariable String countryName) {
        return teamService.getTeamsByCountry(countryName);
    }
}
