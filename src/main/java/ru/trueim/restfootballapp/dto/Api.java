package ru.trueim.restfootballapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Api {

    private Api api;
    private Integer results;
    private List<Team> teams;
    private String version;
    private String warning;

    public Api() {

    }

    public Integer getResults() {
        return results;
    }

    public void setResults(Integer results) {
        this.results = results;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    @Override
    public String toString() {
        StringBuilder strTeams = new StringBuilder();
        if (teams == null) {
            strTeams.append("\"null\",");
        }else {
            teams.forEach(t -> strTeams.append(t).append(","));
        }
        return "Api{" +
                "results=\"" + getResults() + '\"' +
                ",teams=" + strTeams.toString() +
                "version=\"" + getVersion() + '\"' +
                ",warning=\"" + getWarning() + '\"';
    }

    public Api getApi() {
        return api;
    }

    public void setApi(Api api) {
        this.api = api;
    }
}
