package ru.trueim.restfootballapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Team {

    @JsonProperty("team_id")
    private Integer teamId;

    private String name;

    private String code;

    private String logo;

    private String country;
    @JsonProperty("is_national")
    private Boolean isNational;

    private Integer founded;

    @JsonProperty("venue_name")
    private String venueName;

    @JsonProperty("venue_surface")
    private String venueSurface;

    @JsonProperty("venue_address")
    private String venueAddress;

    @JsonProperty("venue_city")
    private String venueCity;

    @JsonProperty("venue_capacity")
    private Integer venueCapacity;

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getFounded() {
        return founded;
    }

    public void setFounded(Integer founded) {
        this.founded = founded;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueSurface() {
        return venueSurface;
    }

    public void setVenueSurface(String venueSurface) {
        this.venueSurface = venueSurface;
    }

    public String getVenueAddress() {
        return venueAddress;
    }

    public void setVenueAddress(String venueAddress) {
        this.venueAddress = venueAddress;
    }

    public String getVenueCity() {
        return venueCity;
    }

    public void setVenueCity(String venueCity) {
        this.venueCity = venueCity;
    }

    public Integer getVenueCapacity() {
        return venueCapacity;
    }

    public void setVenueCapacity(Integer venueCapacity) {
        this.venueCapacity = venueCapacity;
    }

    public Boolean getIsNational() {
        return isNational;
    }

    public void setIsNational(Boolean isNational) {
        this.isNational = isNational;
    }

    @Override
    public String toString() {
        return "team{" +
                "team_id=\"" + getTeamId() + '\"' +
                ",name=" + getName() + '\"' +
                ",code=" + getCode() + '\"' +
                ",logo=" + getLogo() + '\"' +
                ",country=" + getCountry() + '\"' +
                ",founded=" + getFounded() + '\"' +
                ",is_national=" + getIsNational() + '\"' +
                ",venue_name=" + getVenueName() + '\"' +
                ",venue_surface=" + getVenueSurface() + '\"' +
                ",venue_address=" + getVenueAddress() + '\"' +
                ",venue_city=" + getVenueCity() + '\"' +
                ",venue_capacity=" + getVenueCapacity() + '\"';
    }
}
