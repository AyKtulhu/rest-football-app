package ru.trueim.restfootballapp.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.internal.matchers.VarargMatcher;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;
import ru.trueim.restfootballapp.dto.Api;
import ru.trueim.restfootballapp.dto.Team;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static ru.trueim.restfootballapp.Constants.*;

@ExtendWith(MockitoExtension.class)
class DefaultSearchTeamServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private DefaultSearchTeamService testClass;

    @Test
    @DisplayName("Get all Brazil football teams")
    void getCountryTeams() {
        Api api = new Api();
        api.setResults(1);
        api.setTeams(Collections.singletonList(new Team()));
        api.setVersion(VERSION);
        api.setWarning(WARNING);
        Api handler = new Api();
        handler.setApi(api);
        ArrayElementMatcher<String> matcher = new ArrayElementMatcher<>(COUNTRY);
        Mockito.doReturn(handler).when(restTemplate).getForObject(any(), eq(Api.class), argThat(matcher));
        assertEquals(api.getTeams(), testClass.getTeamsByCountry(COUNTRY));
    }

    @Test
    @DisplayName("Get an empty list if the returned data is null")
    void getCountryEmpty() {
        ArrayElementMatcher<String> matcher = new ArrayElementMatcher<>(COUNTRY);
        Mockito.doReturn(null).when(restTemplate).getForObject(any(), eq(Api.class), argThat(matcher));
        assertTrue(testClass.getTeamsByCountry(COUNTRY).isEmpty());
    }

    private static class ArrayElementMatcher<T> implements ArgumentMatcher<T>, VarargMatcher {
        private final T element;

        public ArrayElementMatcher(T element) {
            this.element = element;
        }

        @Override
        public boolean matches(T t) {
            return true;
        }
    }
}
