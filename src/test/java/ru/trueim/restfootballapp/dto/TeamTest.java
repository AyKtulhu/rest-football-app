package ru.trueim.restfootballapp.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static ru.trueim.restfootballapp.Constants.COUNTRY;

class TeamTest {

    @Test
    void test() {
        Team team = new Team();
        team.setCode("Code");
        team.setCountry(COUNTRY);
        team.setFounded(1905);
        team.setIsNational(true);
        team.setTeamId(41);
        team.setVenueAddress("");
        team.setVenueCapacity(2);
        team.setVenueCity("");
        team.setVenueName("Cars Jeans Stadion");
        team.setVenueSurface("artificial turf");
        team.setLogo("");
        team.setName("ADO Den Haag");
        assertNotNull(team.toString());
    }
}