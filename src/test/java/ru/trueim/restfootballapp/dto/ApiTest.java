package ru.trueim.restfootballapp.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static ru.trueim.restfootballapp.Constants.VERSION;
import static ru.trueim.restfootballapp.Constants.WARNING;

class ApiTest {

    @Test
    void testToString() {
        Api api = new Api();
        assertNotNull(api.toString());
    }
}