package ru.trueim.restfootballapp.controllers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.trueim.restfootballapp.dto.Team;
import ru.trueim.restfootballapp.services.SearchTeamService;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static ru.trueim.restfootballapp.Constants.COUNTRY;

@ExtendWith(MockitoExtension.class)
class TeamsResourceTest {

    @Mock
    private SearchTeamService teamService;

    @InjectMocks
    private TeamsResource testClass;

    @Test
    @DisplayName("Get all Brazil football teams")
    void getCountryTeams() {
        Team team = new Team();
        when(teamService.getTeamsByCountry(anyString())).thenReturn(Collections.singletonList(team));
        assertEquals(testClass.getCountryTeams(COUNTRY).get(0), team);
    }
}